<?php
 require_once("clsDB_CsCart.php");

 class clsCsCart{

   private $dbCS;
   public function __construct(){
     $objDB_CS = new clsDB_CsCart();
     $this->dbCS = $objDB_CS->cnnDB();
   }

//====================================================================
 function RegistrarUsrCsCart($sUsr, $sCorreo){
    $sSQL="INSERT INTO cscart_users(user_login, company_id, password, firstname, email, lang_code) ";
    $sSQL.="VALUES('$sUsr', 1, '91ec1f9324753048c0096d036a694f86', '$sUsr', '$sCorreo', 'en')";

   try{

     //echo ("SQL: ".$sSQL);
     $this->dbCS->query("SET NAMES 'utf8'");
     //Preparar transaccion
     //$this->db->beginTransaction();
     $Res = $this->dbCS->query($sSQL);
     //$this->db->commit();
       if ($Res) {
          return 1;
       }
       else {
          return 0;
       }
   }

   catch(PDOException $e){
       // handle PDOException
       echo $e->getMessage();
       return false;
       die();
   }
 }//FIN de Registrar

  //====================================================================
  function ActualizarUsers($sUsr, $sCorreo){
    $sSQL = "UPDATE cscart_users SET ";
    $sSQL .= "email = '$sCorreo' ";
    $sSQL .= "WHERE user_login='$sUsr' ";

  try{

    //echo ("SQL: ".$sSQL);
    //$this->dbCS->query("SET NAMES 'utf8'");
    //Preparar transaccion
    //$this->db->beginTransaction();
    $Res = $this->dbCS->query($sSQL);
    //$this->db->commit();
      if ($Res) {
        return 1;
        //EscribirLog('DB.log', 'SQL:1');
      } else {
        return 0;
        //EscribirLog('DB.log', 'SQL:0');
      }
   }

   catch(PDOException $e){
       //EscribirLog('DB.log', 'ERROR:'.$sSQL);
       // handle PDOException
       //echo $e->getMessage();
       return false;
       die();
   }
 }//FIN de Actualizar users

  //====================================================================
  function ActualizarOrdenStatus($IdO, $sSt){
    $sSQL = "UPDATE cscart_orders SET ";
    $sSQL .= "status = 'P' ";
    $sSQL .= "WHERE order_id=$IdO ";

  try{

    //echo ("SQL: ".$sSQL);
    //EscribirLog('DB.log', $sSQL);
    //$this->dbCS->query("SET NAMES 'utf8'");
    //Preparar transaccion
    //$this->db->beginTransaction();
    $Res = $this->dbCS->query($sSQL);
    //$this->db->commit();
      if ($Res) {
        return 1;
        //EscribirLog('DB.log', 'SQL:1');
      } else {
        return 0;
        //EscribirLog('DB.log', 'SQL:0');
      }
   }

   catch(PDOException $e){
       //EscribirLog('DB.log', 'ERROR:'.$sSQL);
       // handle PDOException
       //echo $e->getMessage();
       return false;
       die();
   }
 }//FIN ActualizarOrdenes


//=======================================
 function EscribirLog($sArchivo, $sLinea){
  date_default_timezone_set('america/mexico_city');
  $sFR = date("Y-m-d H:i:s");

  try{
     $sLinea = 'Fecha: '.$sFR.' --- '.$sLinea;
     $sPath = '/var/www/html/webhoook/DB/Log/'.$sArchivo;
     $file = fopen($sPath, "a+");
     fwrite($file, 'DB:'.$sLinea.PHP_EOL);
     fclose($file);
     return 1;
  }
  catch (Exception $e) {
     return 0;
  }
 }//FIN de la funcion

}//FIN de la Clase

?>
