<?php
 require_once("DB/clsTrans.php");
 require_once("DB/clsCsCart.php");


 date_default_timezone_set('america/mexico_city');
 $sFH = date("Y-m-d H:i:s");

 // Permite la conexion desde cualquier origen
 header("Access-Control-Allow-Origin: *");

 // Permite la ejecucion de los metodos
 header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");

 try{
    $dat = file_get_contents("php://input");
    $data = json_decode($dat);

    if (!empty($data)){
        $sType = $data->type;
        $sED = $data->event_date;
        $sED = str_replace('T', ' ', $sED);
        $sED = substr($sED, 0, 19);
        //$sVC = $data->verification_code;
        $sMonto = $data->transaction->amount;
        $Id = trim($data->transaction->id);
        $sST = trim($data->transaction->status);
        //$sDesc = $data->description;

        $sUsr = 'hugo';
        $sLinea = 'Status: '.$sST.' Id transacción: '.$Id.' Monto: '.$sMonto.' ED:'.$sED;
        //echo('LINEA: '.$sLinea);
        $sPath = './Log/OpenPay.log';
        $file = fopen($sPath, "a+");
        fwrite($file, $sLinea.PHP_EOL);
        fclose($file);

        //Esperar 2 segundos
        usleep(2000000);
        if ($sST == 'completed'){
           //Buscar transaccion
           $objT = new clsTrans();
           $resT = $objT->ExisteTrans('', $Id, '');
           if (isset($resT)){
             foreach($resT as $valT){
               $IdT_OP = $valT['id_TransOP'];
               $sSt = $valT['Estado'];
               $sTipo = $valT['Tipo'];
               $IdO = $valT['id_Orden'];
               $IdUsr = $valT['id_Usr'];
             }
             //EscribirLog('OpenPay.log', ' TIPO:'.$sTipo);
             if ($sTipo == 'CSCART'){
               EscribirLog('CsCart.log', 'PR PROCESAR ORDEN:'.$IdO);
               $resOK = ActualizarOrden($IdO, 'P');

               //ActualizarStatus
               //$objCC = new clsCsCart();
               //$resCC = $objCC->ActualizarOrdenStatus($IdO, 'P');
               //if ($resCC == 1){
               //  EscribirLog('Trans.log', 'OK: '.$IdO.' ORDEN:PAGADA');
               //}
             }

             elseif ($sTipo == 'CSCART_PCT'){
               EscribirLog('CsCart.log', 'PCT PROCESAR ORDEN:'.$IdO);
               $resOK = ActualizarOrden($IdO, 'P');

               //$objCC = new clsCsCart();
               //EscribirLog('CsCart.log', 'CREAR OBJETO');
               //$resCC = $objCC->ActualizarOrdenStatus($IdO, 'P');
               //EscribirLog('CsCart.log', 'OK:'.$resCC);
               //if ($resCC == 1){
               //  EscribirLog('Trans.log', 'OK: '.$IdO.' ORDEN:PAGADA');
               //}
             }

             elseif ($sTipo == 'RECARGA_PORTAL'){
                //Hacer el cargo a GesPage
                $resAS = ActualizarSaldo($sUsr, $sMonto, 'Portal recarga.');
                EscribirLog('OpenPay.log', 'RECARGA_PORTAL Respuesta resAS: '.$resAS);
                if ($resAS == 1){
                   EscribirLog('OpenPay.log', 'LLEGO PARA ACTUALIZAR');
                   $resT = $objT->ActualizarTransStatus($Id, 'COMPLETADO');
                   EscribirLog('OpenPay.log', 'RECARGA_PORTAL Respuesta resT: '.$resT);
                }
             }

             elseif ($sTipo == 'RECARGA_CSCART'){
                EscribirLog('CsCart.log', 'LLEGO RECARGA CSCART');
                //Hacer el cargo a GesPage
                $resAS = ActualizarSaldo($sUsr, $sMonto, 'CsCart recarga.');
             }

             elseif ($sTipo == 'CSCART_R_PCT'){
                $resOK = ActualizarOrden($IdO, 'P');
                EscribirLog('CsCart.log', 'LLEGO RECARGA CSCART_R_PCT');
                $sUsr = ExisteUsrControl($IdUsr, '');
                //Hacer el cargo a GesPage
                $resAS = ActualizarSaldo($sUsr, $sMonto, 'CsCart recarga pct.');
             }

           }//FIN ExisteTransaccion
           else{
              EscribirLog('OpenPay.log', 'TRANSACCION NO ENCONTRADA');
           }

        }
    }//Fin del if INPUT

    header('HTTP/1.1 200 OK');
    http_response_code(200);

 }//FIN del try

 catch (Exception $e) {
    header('HTTP/1.1 400 NO');
    http_response_code(400);

    $sPath = './Log/OpenPay.log';
    $file = fopen($sPath, "a+");
    fwrite($file, 'ERROR:'.$e->getMessage().PHP_EOL);
    fclose($file);
 }


 //=======================================
 function ActualizarOrden($IdO, $sSt){
    $servername = "localhost";
    $cnn = new PDO("mysql:host=$servername;dbname=csb2b", "usr_otro", "TmpTmp");
    //EscribirLog('CsCart.log', 'cnn OK');
    $sSQL = "UPDATE cscart_orders SET ";
    $sSQL .= "status = 'P' ";
    $sSQL .= "WHERE order_id=$IdO ";

    $Res = $cnn->query($sSQL);
      if ($Res) {
        return 1;
       // EscribirLog('Trans.log', 'OK: '.$IdO.' ORDEN:PAGADA');
      }
      else {
        return 0;
      }

 }//FIN del function


 //=======================================
 function EscribirLog($sArchivo, $sLinea){
  date_default_timezone_set('america/mexico_city');
  $sFR = date("Y-m-d H:i:s");

  try{
     $sLinea = 'FechaRecibido: '.$sFR.' --- '.$sLinea;
     $sPath = './Log/'.$sArchivo;
     $file = fopen($sPath, "a+");
     fwrite($file, '-'.$sLinea.PHP_EOL);
     fclose($file);
     return 0;
  }
  catch (Exception $e) {
     $sPath = './Log/Error.log';
     $file = fopen($sPath, "a+");
     fwrite($file, 'ERROR:'.$e->getMessage().PHP_EOL);
     fclose($file);
     return 0;
  }
}//FIN de la funcion

//====================================================
 function ActualizarSaldo($sUsr, $nCargo, $sLog){
  try{
     //EscribirLog('CsCart.log', 'LLEGO ActualizarSaldo '.$sUsr);

     $ch = curl_init();
     curl_setopt($ch, CURLOPT_TIMEOUT, 5);
     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
     curl_setopt($ch, CURLOPT_URL, 'http://74.208.92.108:7180/pserver/rs/public/credentials/usr_api/12345678');
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
     $resC = curl_exec($ch);
     curl_close($ch);
     $sC = json_decode($resC);
     $sSt = $sC->ret_code;
     $sToken = $sC->token;

     if ($sSt == 200){
       //$nCargoOK = ($nCargo * -1);
       $sHeader = array('Content-Type:application/json');
       $sDat = '{"username":"'.$sUsr.'", "cardid":"", "add_balance":'.$nCargo.', "log":"'.$sLog.'"}';

       $ch = curl_init();
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
       curl_setopt($ch, CURLOPT_URL, 'http://74.208.92.108:7180/pserver/rs/public/user_upd_balance/'.$sToken);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $sHeader);
       curl_setopt($ch, CURLOPT_POSTFIELDS, $sDat);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       $resOK = curl_exec($ch);
       curl_close($ch);
       //var_dump($resOK);
       $sDatosU = json_decode($resOK);
       $sSt = $sDatosU->ret_code;
       $nSaldo = $sDatosU->user_credit;
       if ($sSt == 200){
         // echo("Saldo: ".$nSaldo.PHP_EOL);
         EscribirLog('GesPage.log', ' OK Usr:'.$sUsr.' Monto:'.$nCargo.' Saldo:'.$nSaldo);
         return 1;
       }
       else{
         EscribirLog('GesPage.log', 'NO St:'.$sSt.' Usr:'.$sUsr.' Monto:'.$nCargo.' Saldo:'.$nSaldo);
         return 0;
       }
     }
     else{
       return 0;
     }

  }
   catch(ErrorSCB $e) {
      //echo('ERROR.');
      $pp_response['order_status'] = 'F';
      $pp_response['reason_text'] = $e->getErrorCode() . ': ' . $e->getDescription();
      fn_set_notification('E', __('error'),  $e->getErrorCode() . ': ' . $e->getDescription());
      return 0;
   }
 }//FIN funcion ConsultarSaldo

//=======================================
 function ExisteUsrControl($IdUsr, $sCorreo){
   require_once("DB/clsUsrControl.php");
   $sUsr = '';

   try{
     $objUsr = new clsUsrControl();
     $resU = $objUsr->ExisteUsrControl($IdUsr, '', $sCorreo);
     if (isset($resU)){
        foreach($resU as $Item){
          $sUsr = $Item['UserGP'];
          //$IdUsr = $Item['id_Usr'];
        }
     }
     return $sUsr;
  }
  catch (Exception $e) {
     $sPath = '/var/www/html/app/payments/Log/Error.log';
     $file = fopen($sPath, "a+");
     fwrite($file, 'ERROR:'.$e->getMessage().PHP_EOL);
     fclose($file);
     return '';
  }
}//FIN de la funcion


?>
