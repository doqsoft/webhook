<?php
 require_once("clsDB.php");

 class clsTrans{

   private $Tmp;
   private $db;
   public function __construct(){
     $objDB = new clsDB();
     $this->db = $objDB->CnnDB();
   }
//====================================================================

 function ExisteTrans($IdT, $IdTransOP, $IdUsr){
    $sSQL="SELECT * FROM Transaccion ";
    $sSQL.="WHERE (Transaccion.Estado<>'COMPLETADO') ";

    if ($IdT!=''){
      $sSQL.=" AND (id_T=$IdT) ";

    }
    elseif ($IdTransOP !=''){
     $sSQL.=" AND (id_TransOP='$IdTransOP') ";
    }

    elseif ($IdUsr != ''){
      $sSQL.=" AND (id_Usr=$IdUsr) ";
    }

   //echo ('SQL:'.$sSQL );
   //EscribirLog('DB.log', 'SQL: '.$sSQL);

   foreach ($this->db->query($sSQL) as $Res) {
      $this->Tmp[] = $Res;
   }
   return $this->Tmp;
   $this->db = null;
 }

//=======================================
 function EscribirLog($sArchivo, $sLinea){
  date_default_timezone_set('america/mexico_city');
  $sFR = date("Y-m-d H:i:s");

  try{
     $sLinea = 'Fecha: '.$sFR.' --- '.$sLinea;
     $sPath = '/var/www/html/pagar.dnsalias.org/myWebhook/DB/'.$sArchivo;
     $file = fopen($sPath, "a+");
     fwrite($file, 'DB:'.$sLinea.PHP_EOL);
     fclose($file);
     return 0;
  }
  catch (Exception $e) {
     return 0;
  }
}//FIN de la funcion



 //======================================
  function ActualizarTransStatus($IdT, $sSt){
    $sSQL = "UPDATE Transaccion SET ";
    $sSQL .= "Estado = '$sSt' ";
    $sSQL .= "WHERE id_TransOP='$IdT' ";

  try{

    //echo ("SQL: ".$sSQL);
    //EscribirLog('DB.log', 'SQL: '.$sSQL);

    $this->db->query("SET NAMES 'utf8'");
    //Preparar transaccion
    //$this->db->beginTransaction();
    $Res = $this->db->query($sSQL);
    //echo ("Res:" .$Res);
    //$this->db->commit();
      if ($Res) {
         return 1;
      } else {
         return 0;
      }
   }
   catch(PDOException $e){
       // handle PDOException
       echo $e->getMessage();
       return 0;
   }
}//FIN de la funcion


}//FIN de la Clase

?>
