<?php
 require_once("clsDB_Ctrl.php");

 class clsUsrControl{

   private $Tmp;
   private $db;
   public function __construct(){
     $objDB = new clsDB_Ctrl();
     $this->db = $objDB->CnnDB();
   }
//====================================================================
 function RegistrarUsrControl($UsrGP, $sCorreo, $sCorreoCS, $sPC){
    $sSQL="INSERT INTO Usuario(UserGP, Correo, CorreoCsCart, Estado, PrintCode) ";
    $sSQL.="VALUES('$UsrGP', '$sCorreo', '$sCorreoCS', 'ACTIVO', '$sPC')";

   try{

     //echo ("SQL: ".$sSQ.PHP_EOLL);
     $this->db->query("SET NAMES 'utf8'");
     //Preparar transaccion
     //$this->db->beginTransaction();
     $Res = $this->db->query($sSQL);
     //$this->db->commit();
       if ($Res) {
          return 1;
       }
       else {
          return 0;
       }
   }

   catch(PDOException $e){
       // handle PDOException
       echo $e->getMessage();
       return false;
       die();
   }
 }//FIN de Registrar


 function ExisteUsrControl($Id, $UsrGP, $sCorreo){
    $sSQL="SELECT DISTINCT * FROM Usuario ";
    $sSQL.="WHERE (Estado != 'CANCELADO') ";

    if ($Id!=''){
      $sSQL.=" AND (id_Usr = $Id) ";

    }
    elseif ($UsrGP !=''){
     $sSQL.=" AND (UserGP='$UsrGP') ";
    }

    elseif ($sCorreo != ''){
      $sSQL.=" AND (Correo='$sCorreo') ";
    }

   //echo ('SQL:'.$sSQL.PHP_EOL );
   //EscribirLog('DB.log', 'SQL: '.$sSQL);

   foreach ($this->db->query($sSQL) as $Res) {
      $this->Tmp[] = $Res;
   }
   return $this->Tmp;
   $this->db = null;
 }

//=======================================
 function EscribirLog($sArchivo, $sLinea){
  date_default_timezone_set('america/mexico_city');
  $sFR = date("Y-m-d H:i:s");

  try{
     $sLinea = 'Fecha: '.$sFR.' --- '.$sLinea;
     $sPath = '/var/www/html/webhook/DB/'.$sArchivo;
     $file = fopen($sPath, "a+");
     fwrite($file, 'DB:'.$sLinea.PHP_EOL);
     fclose($file);
     return 0;
  }
  catch (Exception $e) {
     return 0;
  }
}//FIN de la funcion



 //======================================
  function ActualizarUsrCtrl($Id, $sUsrGP, $sCorreo){
        $sSQL = "UPDATE Usuario SET ";
        $sSQL .= "UserGP = '$sUsrGP', ";
        $sSQL .= "Correo = '$sCorreo' ";
        $sSQL .= "WHERE id_Usr=$Id ";

  try{
    //echo ("UsrCtrl_SQL: ".$sSQL);
    //EscribirLog('DB.log', 'SQL: '.$sSQL);
    $this->db->query("SET NAMES 'utf8'");
    //Preparar transaccion
    //$this->db->beginTransaction();
    $Res = $this->db->query($sSQL);
    //$this->db->commit();
      if ($Res) {
         return 1;
      } else {
         return 0;
      }
   }
   catch(PDOException $e){
       // handle PDOException
       echo $e->getMessage();
       return 0;
   }
}//FIN de la funcion


}//FIN de la Clase

?>
